﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MrTimelapse.Commands
{
  public class CreateTimelapseCommand : ICommand
  {
    private readonly MainViewModel _viewModel;

    public CreateTimelapseCommand(MainViewModel viewModel)
    {
      _viewModel = viewModel;
      _viewModel.IsTimelapseBusy = false;
    }

    public bool CanExecute(object parameter)
    {
      return !_viewModel.IsTimelapseBusy && _viewModel.TimelapseTabEnabled && _viewModel.Photos.Any()
             && _viewModel.FramesPerSecond > 0 && _viewModel.ResolutionHeight > 0 && _viewModel.ResolutionWidth > 0;
    }

    public async void Execute(object parameter)
    {
      _viewModel.IsTimelapseBusy = true;
      await _viewModel.CreateTimelapse();
      _viewModel.IsTimelapseBusy = false;
    }

    public void FireCanExecuteChanged()
    {
      CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

    public event EventHandler CanExecuteChanged;

  }
}
