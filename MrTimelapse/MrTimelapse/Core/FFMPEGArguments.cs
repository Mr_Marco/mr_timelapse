﻿using System.Collections.Generic;
using System.Text;

namespace MrTimelapse.Core
{
  public class FFMPEGArguments
  {
    private static string _forceInputOutput = "-f";
    private static string _imageFileDemuxer = "image2";
    private static string _framesPerSecond = "-r";
    private static string _startNumber = "-start_number";
    private static string _inputFiles = "-i";
    private static string _numberOfFiles = "-frames:v";
    private static string _videoCodec = "-vcodec";
    private static string _resolution = "-s";

    private Dictionary<string, string> _arguments;

    private string _outputFileName;
    private int _outputVideoFPS;

    public FFMPEGArguments()
    {
      _arguments = new Dictionary<string, string>();
      _outputVideoFPS = 30;
    }

    public void ClearArguments()
    {
      _arguments.Clear();
    }

    private bool _TryAdd(string key, string value)
    {
      if (_arguments.ContainsKey(key))
      {
        return false;
      }
      _arguments.Add(key, value);
      return true;
    }

    public bool AddImageFileDemuxer()
    {
      return _TryAdd(_imageFileDemuxer, string.Empty);
    }

    public bool AddFramesPerSecond(int framesPerSecond)
    {
      return _TryAdd(_framesPerSecond, framesPerSecond.ToString());
    }

    public bool AddStartNumber(int startNumber)
    {
      return _TryAdd(_startNumber, startNumber.ToString());
    }

    public bool AddInputFiles(string baseName, int numberingLength, string fileExtension)
    {
      string formattedNumbering = $"\"{baseName}%{numberingLength.ToString("D2")}d{fileExtension}\"";
      return _TryAdd(_inputFiles, formattedNumbering);
    }

    public bool AddNumberOfFiles(int numberOfFiles)
    {
      return _TryAdd(_numberOfFiles, numberOfFiles.ToString());
    }

    public bool AddLibx264Encoder()
    {
      return _TryAdd(_videoCodec, "libx264");
    }

    public bool AddResolution(int width, int height)
    {
      string formattedResolution = $"{width}x{height}";
      return _TryAdd(_resolution, formattedResolution);
    }

    public void SetOutputFileName(string outputFileName)
    {
      _outputFileName = outputFileName;
    }

    public void SetOutputVideoFPS(int framesPerSecond)
    {
      _outputVideoFPS = framesPerSecond;
    }

    public string CreateFFMPEGArguments()
    {
      StringBuilder arguments = new StringBuilder();
      foreach (KeyValuePair<string, string> keyValuePair in _arguments)
      {
        arguments.Append(keyValuePair.Key);
        arguments.Append(" ");
        if (!string.IsNullOrEmpty(keyValuePair.Value))
        {
          arguments.Append(keyValuePair.Value);
          arguments.Append(" ");
        }
      }
      string frameRate;
      if (_arguments.TryGetValue(_framesPerSecond, out frameRate))
      {
          arguments.Append($"{ _framesPerSecond} {_outputVideoFPS}");
      }
      arguments.Append("");
      arguments.Append(" ");
      arguments.Append(_outputFileName);
      return arguments.ToString();
    }
  }
}
