﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Int32;

namespace MrTimelapse.Core
{
  //G0010001.JPG
  //G0020001.JPG
  //G0020001.JPG
  //-r: frames per second
  //-start_number: start number of the numeration
  //-i: file name pattern %03d means that the number has a length of 3
  //-frames:v number of files
  //-s: resolution wxh
  //ffmpeg -f image2 -r 12 -start_number 002 -i G0010%03d.JPG -frames:v 500 -vcodec libx264 -s 1920×1080 timelapse_1080P.mp4

  public class TimelapseCore
  {
    private string _ffmpegPath;
    private int? _framesPerSecond;
    private int _outputVideoFPS;
    private int? _width;
    private int? _height;
    private string _outputPath;

    private FFMPEGArguments _arguments;

    public event EventHandler<string> LogDataReceived;
    public event EventHandler<int> ReportProgress;

    public TimelapseCore()
    {
      _outputPath = string.Empty;
      _ffmpegPath = string.Empty;
      _arguments = new FFMPEGArguments();
      _width = null;
      _height = null;
      _outputVideoFPS = 30;
    }

    public void SetOutputPathAndFFMPEGPath(string outputPath, string ffmpegPath)
    {
      _outputPath = outputPath;
      _ffmpegPath = ffmpegPath;
    }


    public void SetFramesPerSecond(int framesPerSecond)
    {
      _framesPerSecond = framesPerSecond;
    }

    public void SetResolution(int width, int height)
    {
      _width = width;
      _height = height;
    }

    public void CreateTimelapse(IEnumerable<string> images)
    {
      if (string.IsNullOrEmpty(_ffmpegPath))
      {
        return;
      }
      IOrderedEnumerable<string> orderedImages = images.OrderBy(x => x);
      string baseName = GetBaseName(orderedImages);
      string firstWithBase = orderedImages.First(x => x.Contains(baseName));
      string fileExtension = Path.GetExtension(firstWithBase)?.ToLower();
      int numberingLength = Path.GetFileNameWithoutExtension(firstWithBase.Replace(baseName, string.Empty)).Length;
      int? startNumber = null;
      int parsedNumber;
      if (TryParse(
        Path.GetFileNameWithoutExtension(firstWithBase.Replace(baseName, string.Empty)),
        out parsedNumber))
      {
        startNumber = parsedNumber;
      }
      if (!_width.HasValue || !_height.HasValue)
      {
        int width, height;
        if (ImageHandler.GetImageResolution(firstWithBase, out width, out height))
        {
          _width = width;
          _height = height;
        }
      }
      if (!_framesPerSecond.HasValue)
      {
        _framesPerSecond = 15;
      }
      string arguments = _CreateArguments(_framesPerSecond.Value, null, startNumber, _width.Value, _height.Value, baseName, numberingLength,
        fileExtension, _outputPath);

      int totalNumberOfFrames = (int)((double) images.Count(x => x.Contains(baseName))/_framesPerSecond.Value*_outputVideoFPS);

      _RunFFMPEG(arguments, totalNumberOfFrames);
    }

    public string GetBaseName(IEnumerable<string> images)
    {
      if(images == null || !images.Any())
      {
        return string.Empty;
      }
      Dictionary<string, int> baseNamesAndNumberOfOccurences = new Dictionary<string, int>();

      foreach (string image in images)
      {
        for (int i = 0; i < image.Length; i++)
        {
          string subString = image.Substring(0, i + 1);
          if (!baseNamesAndNumberOfOccurences.ContainsKey(subString))
          {
            baseNamesAndNumberOfOccurences.Add(subString, 0);
          }
          baseNamesAndNumberOfOccurences[subString]++;
        }
      }
      int maxNumberOfOccurences = baseNamesAndNumberOfOccurences.Select(x => x.Value).Max();
      if (maxNumberOfOccurences < 2)
      {
        return Guid.NewGuid().ToString();
      }
      List<string> subStringWithMaxNumberOfOccurences = baseNamesAndNumberOfOccurences.Where(x => x.Value == maxNumberOfOccurences).Select(x => x.Key).ToList();

      int lengthOfLongestStringWithMaxNumberOfOccurences = subStringWithMaxNumberOfOccurences.Max(x => x.Length);
      return subStringWithMaxNumberOfOccurences.First(x => x.Length == lengthOfLongestStringWithMaxNumberOfOccurences);
    }

    private string _CreateArguments(int framesPerSecond, int? numberOfFiles, int? startNumber, int width, int height,
      string baseName, int numberingLength, string imageExtension, string outputPath)
    {
      _arguments.ClearArguments();
      DateTime now = DateTime.Now;
      string nameExtension = $"{now.Year.ToString("D4")}{now.Month.ToString("D2")}{now.Day.ToString("D2")}{now.Hour.ToString("D2")}{now.Minute.ToString("D2")}{now.Second.ToString("D2")}";
      string outputFileName = Path.Combine(outputPath, $"timelapse_{nameExtension}.mp4");
      _arguments.SetOutputFileName(outputFileName);
      //_arguments.AddImageFileDemuxer();
      _arguments.AddFramesPerSecond(framesPerSecond);
      if (startNumber.HasValue)
      {
        _arguments.AddStartNumber(startNumber.Value);
      }
      _arguments.AddInputFiles(baseName, numberingLength, imageExtension);
      if (numberOfFiles.HasValue)
      {
        _arguments.AddNumberOfFiles(numberOfFiles.Value);
      }
      _arguments.AddLibx264Encoder();
      _arguments.AddResolution(width, height);
      return _arguments.CreateFFMPEGArguments();
    }

    private bool _RunFFMPEG(string arguments, int totalNumberOfFrames)
    {
      ProcessStartInfo processStartInfo = new ProcessStartInfo()
      {
        CreateNoWindow = true,
        WindowStyle = ProcessWindowStyle.Hidden,
        UseShellExecute = false,
        FileName = _ffmpegPath,
        Arguments = arguments
      };
      try
      {
        int progress = 0;

        StringBuilder sb = new StringBuilder();
        string baseString = "---------- {0} ----------";
        Process p = new Process();
        p.StartInfo = processStartInfo;


        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.RedirectStandardError = true;

        p.OutputDataReceived += (sender, args) =>
        {
          //sb.AppendLine(string.Format(baseString, "FFMPEG output data"));
          sb.AppendLine(args.Data);
          //sb.AppendLine();
          _FireLogDataReceived(sb.ToString());
        };
        p.ErrorDataReceived += (sender, args) =>
        {
          int newProgress;
          if (_CalculateProgress(args.Data, totalNumberOfFrames, out newProgress))
          {
            progress = newProgress;
            ReportProgress?.Invoke(this, progress);
          }
          //sb.AppendLine(string.Format(baseString, "FFMPEG error data"));
          sb.AppendLine(args.Data);
          //sb.AppendLine();
          _FireLogDataReceived(sb.ToString());
        };
        p.Start();

        sb.AppendLine(string.Format(baseString, "Timelapse creation started"));
        sb.AppendLine();
        _FireLogDataReceived(sb.ToString());

        p.BeginOutputReadLine();
        p.BeginErrorReadLine();
        p.WaitForExit();

        sb.AppendLine(string.Format(baseString, "Timelapse creation completed"));
        _FireLogDataReceived(sb.ToString());


#if DEBUG
        Console.WriteLine(sb.ToString());
#endif
        return true;
      }
      catch (Exception exception)
      {
        return false;
      }
    }

    private bool _CalculateProgress(string ffmpegOutput, int totalNumberOfframes, out int progress)
    {
      string frame = "frame=";
      if (string.IsNullOrEmpty(ffmpegOutput) || !ffmpegOutput.StartsWith(frame))
      {
        progress = 0;
        return false;
      }
      int castedFrameNumber = Parse(Regex.Match(ffmpegOutput, @"\d+").Value);
      progress = 100*castedFrameNumber/totalNumberOfframes;
      return true;
    }

    private void _FireLogDataReceived(string outputData)
    {
      LogDataReceived?.Invoke(this, outputData);
    }

  }
}
