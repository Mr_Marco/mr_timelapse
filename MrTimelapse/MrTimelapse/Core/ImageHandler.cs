﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MrTimelapse.Core
{
  public static class ImageHandler
  {
    public static string[] ImageFormats = { ".bmp", ".gif", ".jpeg", ".jpg", ".png", ".tiff" };

    private enum ExposureProgramm
    {
      NotDefined,
      Manual,
      NormalProgram,
      AperturePriority,
      ShutterPriority,
      CreativeProgram,
      ActionProgram,
      PortraitMode,
      LandscapeMode
    }

    private static Dictionary<uint, string> _apexAperture = new Dictionary<uint, string>
    {
      {0, "1"},
      {1, "1.4"},
      {2, "2"},
      {3, "2.8"},
      {4, "4"},
      {5, "5.6"},
      {6, "8"},
      {7, "11"},
      {8, "16"},
      {9, "22"},
      {10, "32"}
    };

    public static Dictionary<int, string> ImageProperties = new Dictionary<int, string>
    {
      /*{0x0131, "Date Taken"},*/
      {0x013B, "Author"},
      {0x503B, "Copyright"},
      {0x0100, "Width"},
      {0x0101, "Height"},
      {0x010F, "Camera maker"},
      {0x0110, "Camera Model"},
      {0x9202, "Aperture"},
      {0x829A, "Exposure Time"},
      {0x8827, "ISO"},
      {0x920A, "Focal Length"},
      {0x9209, "Flash"},
      {0x8822, "Exposure Program"}
    };

    public static bool GetImageResolution(string imagePath, out int width, out int height)
    {
      if (!File.Exists(imagePath) || !ImageFormats.Contains(Path.GetExtension(imagePath)?.ToLower()))
      {
        width = 0;
        height = 0;
        return false;
      }
      using (Image i = Image.FromFile(imagePath))
      {
        width = i.Width;
        height = i.Height;
      }
      return true;
    }

    public static bool GetImageProperties(string imagePath, out string imageProperties)
    {
      if (!File.Exists(imagePath) || !ImageFormats.Contains(Path.GetExtension(imagePath)?.ToLower()))
      {
        imageProperties = string.Empty;
        return false;
      }

      StringBuilder stringBuilder = new StringBuilder();
      using (Image i = Image.FromFile(imagePath))
      {
        foreach (PropertyItem propertyItem in i.PropertyItems.Where(x => ImageProperties.Select(d => d.Key).Contains(x.Id)))
        {
          Tuple<string, string> imageProperty = _CastPropertyItemToString(propertyItem);
          stringBuilder.AppendLine($"{imageProperty.Item1}: {imageProperty.Item2}");
        }
        imageProperties = stringBuilder.ToString();
      }
      return true;
    }

    private static string _CastFStopFromApex(int propertyId, string value)
    {
      if (propertyId != ImageProperties.First(x => x.Value == "Aperture").Key)
      {
        return value;
      }
      uint apexValue;
      if (!uint.TryParse(value, out apexValue))
      {
        return string.Empty;
      }
      string fStop;
      if (!_apexAperture.TryGetValue(apexValue, out fStop))
      {
        return string.Empty;
      }
      return $"f / {fStop}";
    }

    private static string _CastExposureProgram(int propertyId, string value)
    {
      if (propertyId != 0x8822)
      {
        return value;
      }
      int prog;
      if (!int.TryParse(value, out prog))
      {
        return string.Empty;
      }
      if (!Enum.IsDefined(typeof (ExposureProgramm), prog))
      {
        return string.Empty;
      }
      return Enum.GetName(typeof (ExposureProgramm), prog);
    }

    private static string _CastFlash(int propertyId, string value)
    {
      if (propertyId != 0x9209)
      {
        return value;
      }
      int flash;
      if (!int.TryParse(value, out flash))
      {
        return string.Empty;
      }
      return (flash & 1) != 0 ? "Fired" : "Not fired";
    }

    private static string _CastFocalLength(int propertyId, string value)
    {
      if (propertyId != 0x920A)
      {
        return value;
      }
      return $"{value} mm";
    }

    private static Tuple<string, string> _CastPropertyItemToString(PropertyItem propertyItem)
    {
      string value;
      if (propertyItem.Type == 3)
      {
        value = BitConverter.ToUInt16(propertyItem.Value, 0).ToString();
      }
      else if (propertyItem.Type == 4)
      {
        value = BitConverter.ToUInt32(propertyItem.Value, 0).ToString();
      }
      else if (propertyItem.Type == 7)
      {
        value = BitConverter.ToInt32(propertyItem.Value, 0).ToString();
      }
      else if (propertyItem.Type == 5)
      {
        uint nominator = BitConverter.ToUInt32(propertyItem.Value, 0);
        uint denominator = BitConverter.ToUInt32(propertyItem.Value, 4);
        double d = (double)nominator / denominator;
        value = d < 1 ? $"1 / {1 / d}" : (nominator / denominator).ToString();
      }
      else if (propertyItem.Type == 10)
      {
        int nominator = BitConverter.ToInt32(propertyItem.Value, 0);
        int denominator = BitConverter.ToInt32(propertyItem.Value, 4);
        double d = (double)nominator / denominator;
        value = d < 1 ? $"1 / {1 / d}" : (nominator / denominator).ToString();
      }
      else
      {
        value = Encoding.ASCII.GetString(propertyItem.Value).Replace("\0", string.Empty);
      }
      value = _CastExposureProgram(propertyItem.Id, value);
      value = _CastFStopFromApex(propertyItem.Id, value);
      value = _CastFocalLength(propertyItem.Id, value);
      value = _CastFlash(propertyItem.Id, value);
      return new Tuple<string, string>(ImageProperties[propertyItem.Id], value);
    }
  }
}
