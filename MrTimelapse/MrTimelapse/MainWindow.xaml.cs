﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using MrTimelapse.Core;
using DataFormats = System.Windows.DataFormats;
using DragDropEffects = System.Windows.DragDropEffects;
using DragEventArgs = System.Windows.DragEventArgs;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using TextBox = System.Windows.Controls.TextBox;

namespace MrTimelapse
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private MainViewModel _viewModel;
    private bool _isDropAllowed;

    public MainWindow()
    {
      InitializeComponent();
      _viewModel = new MainViewModel();
      DataContext = _viewModel;
      Loaded += (sender, args) => _SettingsTab.IsSelected = !_viewModel.TimelapseTabEnabled;
    }

    private void _OnBrowseOutputPathClick(object sender, RoutedEventArgs e)
    {
      using (var dialog = new FolderBrowserDialog())
      {
        DialogResult result = dialog.ShowDialog();
        if (result != System.Windows.Forms.DialogResult.Cancel)
        {
          _viewModel.OutputPath = dialog.SelectedPath;
        }

      }
    }

    private void _OnBrowseFFMPEGPathClick(object sender, RoutedEventArgs e)
    {
      using (var dialog = new OpenFileDialog())
      {
        dialog.Multiselect = false;
        DialogResult result = dialog.ShowDialog();
        if (result != System.Windows.Forms.DialogResult.Cancel)
        {
          _viewModel.FFMPEGPath = dialog.FileName;
        }

      }
    }



    private void _OnDragEnter(object sender, DragEventArgs e)
    {
      _isDropAllowed = false;
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
      {
        string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
        _isDropAllowed = files.Select(System.IO.Path.GetExtension).Select(x => x.ToLower()).Any(x => ImageHandler.ImageFormats.Contains(x)) &&
          files.Any(x => !_viewModel.Photos.Contains(x));
      }
      e.Effects = _isDropAllowed ? DragDropEffects.All : DragDropEffects.None;
      e.Handled = true;
    }

    private void _OnDragLeave(object sender, DragEventArgs e)
    {
      _isDropAllowed = false;
      e.Effects = DragDropEffects.None;
    }

    private void _OnDragOver(object sender, DragEventArgs e)
    {
      e.Effects = _isDropAllowed ? DragDropEffects.All : DragDropEffects.None;
      e.Handled = true;
    }

    private void _OnDrop(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
      {
        string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
        _AddPhotos(files);

      }
    }

    private void _OnMenuItemDeleteClick(object sender, RoutedEventArgs e)
    {
      List<string> selectedItems = _photoListBox.SelectedItems.OfType<string>().ToList();
      foreach (string selectedItem in selectedItems)
      {
        _viewModel.Photos.Remove(selectedItem);
      }
    }

    private void _OnLogTextBoxChanged(object sender, TextChangedEventArgs e)
    {
      TextBox textBox = sender as TextBox;
      textBox?.ScrollToEnd();
    }

    private void _OnAddPhotosClick(object sender, RoutedEventArgs e)
    {
      using (var dialog = new OpenFileDialog())
      {
        dialog.Multiselect = true;
        DialogResult result = dialog.ShowDialog();
        if (result != System.Windows.Forms.DialogResult.Cancel)
        {
          _AddPhotos(dialog.FileNames.Where(x => ImageHandler.ImageFormats.Contains(Path.GetExtension(x)?.ToLower())));
        }

      }
    }

    private void _AddPhotos(IEnumerable<string> files)
    {
      foreach (string file in files.Where(x => !_viewModel.Photos.Contains(x)).ToList())
      {
        _viewModel.Photos.Add(file);
      }
      List<string> orderedPhotos = _viewModel.Photos.OrderBy(x => x).ToList();
      _viewModel.Photos.Clear();
      foreach (string photo in orderedPhotos)
      {
        _viewModel.Photos.Add(photo);
      }
      _viewModel.GetNewBaseName();
      _viewModel.AutoSetTimelapseValues();
    }

    private void _OnPhotoListBoxKeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key != Key.Delete)
      {
        return;
      }
      _OnMenuItemDeleteClick(sender, e);
    }
  }
}
