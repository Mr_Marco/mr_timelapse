﻿using MrTimelapse.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MrTimelapse.Annotations;
using MrTimelapse.Commands;
using MrTimelapse.Controls;
using MrTimelapse.Properties;

namespace MrTimelapse
{
  public class MainViewModel : INotifyPropertyChanged
  {
    private TimelapseCore _timelapseCore;
    private bool _timelapseTabEnabled;
    private string _length;
    private int _framesPerSecond;
    private int _resolutionWidth;
    private int _resolutionHeight;
    private string _selectedImage;
    private string _imageProperties;
    private string _baseName;
    private bool _isTimelapseBusy;
    private string _logData;
    private double _progressSpinnerScale;

    public MainViewModel()
    {

      BusySpinnerViewModel = new BusySpinnerViewModel();
      _timelapseTabEnabled = !string.IsNullOrEmpty(FFMPEGPath) && !string.IsNullOrEmpty(OutputPath);
      _logData = string.Empty;
      _timelapseCore = new TimelapseCore();
      _timelapseCore.SetOutputPathAndFFMPEGPath(OutputPath, FFMPEGPath);
      _timelapseCore.LogDataReceived += (sender, s) => LogData = s;
      _timelapseCore.ReportProgress += (sender, i) => BusySpinnerViewModel.SpinnerScale = (100 - i)/100.0;
      Photos = new ObservableCollection<string>();
      Photos.CollectionChanged += (sender, args) => CreateTimelapseCommand?.FireCanExecuteChanged();
      _isTimelapseBusy = false;
      CreateTimelapseCommand = new CreateTimelapseCommand(this);
    }

    public BusySpinnerViewModel BusySpinnerViewModel { get; private set; }

    public bool IsTimelapseBusy
    {
      get { return _isTimelapseBusy; }
      set
      {
        if (value == _isTimelapseBusy) return;
        _isTimelapseBusy = value;
        CreateTimelapseCommand?.FireCanExecuteChanged();
        BusySpinnerViewModel.IsBusy = value;
        OnPropertyChanged();
      }
    }

    public string GetNewBaseName()
    {
      BaseName = _timelapseCore.GetBaseName(Photos.Select(Path.GetFileName));
      return BaseName;
    }

    public void AutoSetTimelapseValues()
    {
      string first = Photos.FirstOrDefault(x => x.Contains(BaseName));
      if (string.IsNullOrEmpty(first))
      {
        return;
      }
      int width, height;
      if (ImageHandler.GetImageResolution(first, out width, out height))
      {
        while (width >= 6000 || height >= 4000)
        {
          width = width/2;
          height = height/2;
        }
        ResolutionWidth = width;
        ResolutionHeight = height;
      }
      List<string> list = Photos.Where(x => x.Contains(BaseName)).ToList();
      if (list.Count > 1 && list.Count < 10)
      {
        FramesPerSecond = 1;
      }
      else if (list.Count > 10 && list.Count < 60)
      {
        FramesPerSecond = list.Count/2;
      }
      else if (list.Count > 60)
      {
        FramesPerSecond = 12;
      }

    }


    public ObservableCollection<string> Photos { get; }

    public string LogData
    {
      get { return _logData; }
      private set
      {
        if (value == _logData) return;
        _logData = value;
        OnPropertyChanged();
      }
    }

    public string BaseName
    {
      get { return _baseName; }
      private set
      {
        if (value == _baseName) return;
        _baseName = value;
        OnPropertyChanged();
      }
    }

    public CreateTimelapseCommand CreateTimelapseCommand { get; private set; }

    public string SelectedImage
    {
      get { return _selectedImage; }
      set
      {
        if (value == _selectedImage) return;
        _selectedImage = value;
        Task.Run(() => _GetImageProperties(value));
      }
    }

    private void _GetImageProperties(string imagePath)
    {
      string imageProperties;
      if (ImageHandler.GetImageProperties(imagePath, out imageProperties))
      {
        ImageProperties = imageProperties;
      }
    }

    public string ImageProperties

    {
      get { return _imageProperties; }
      set
      {
        if (value == _imageProperties) return;
        _imageProperties = value;
        OnPropertyChanged();
      }
    }

    public bool TimelapseTabEnabled
    {
      get { return _timelapseTabEnabled; }
      private set
      {
        if (value == _timelapseTabEnabled) return;
        _timelapseTabEnabled = value;
        OnPropertyChanged();
      }
    }

    public int ResolutionWidth
    {
      get { return _resolutionWidth; }
      set
      {
        if (value == _resolutionWidth) return;
        _resolutionWidth = value;
        OnPropertyChanged();
        CreateTimelapseCommand?.FireCanExecuteChanged();
      }
    }

    public int ResolutionHeight
    {
      get { return _resolutionHeight; }
      set
      {
        if (value == _resolutionHeight) return;
        _resolutionHeight = value;
        OnPropertyChanged();
        CreateTimelapseCommand?.FireCanExecuteChanged();
      }
    }

    public int FramesPerSecond
    {
      get { return _framesPerSecond; }
      set
      {
        if (value == _framesPerSecond) return;
        _framesPerSecond = value;
        int numberOfPhotos = Photos.Count(x => Path.GetFileName(x).Contains(BaseName));
        TimeSpan timeSpan = new TimeSpan(0,0,numberOfPhotos/_framesPerSecond);
        Length = timeSpan.ToString("g");
        OnPropertyChanged();
        CreateTimelapseCommand?.FireCanExecuteChanged();
      }
    }

    public string Length
    {
      get { return _length; }
      private set
      {
        if (value == _length) return;
        _length = value;
        OnPropertyChanged();
      }
    }

    public string FFMPEGPath
    {
      get { return Settings.Default.FFMPEGPath; }
      set
      {
        if (Settings.Default.FFMPEGPath == value || (!File.Exists(value) && !string.IsNullOrEmpty(value))) return;
        Settings.Default.FFMPEGPath = value;
        Settings.Default.Save();
        _timelapseCore.SetOutputPathAndFFMPEGPath(OutputPath, value);
        TimelapseTabEnabled = !string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(OutputPath);
        OnPropertyChanged();
        CreateTimelapseCommand?.FireCanExecuteChanged();
      }
    }

    public string OutputPath
    {
      get { return Settings.Default.OutputPath; }
      set
      {
        if (Settings.Default.OutputPath == value || (!Directory.Exists(value) && !string.IsNullOrEmpty(value))) return;
        Settings.Default.OutputPath = value;
        Settings.Default.Save();
        _timelapseCore.SetOutputPathAndFFMPEGPath(value, FFMPEGPath);
        TimelapseTabEnabled = !string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(FFMPEGPath);
        OnPropertyChanged();
        CreateTimelapseCommand?.FireCanExecuteChanged();
      }
    }

    public async Task CreateTimelapse()
    {
      _timelapseCore.SetResolution(ResolutionWidth, ResolutionHeight);
      _timelapseCore.SetFramesPerSecond(FramesPerSecond);
      await Task.Run(() => _timelapseCore.CreateTimelapse(Photos));
    }

    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
