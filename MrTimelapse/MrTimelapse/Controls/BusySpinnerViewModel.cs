﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MrTimelapse.Annotations;

namespace MrTimelapse.Controls
{
  public class BusySpinnerViewModel : INotifyPropertyChanged
  {
    private bool _isBusy;
    private double _spinnerScale;
    public event PropertyChangedEventHandler PropertyChanged;

    public bool IsBusy
    {
      get { return _isBusy; }
      set
      {
        if (value == _isBusy) return;
        _isBusy = value;
        OnPropertyChanged();
      }
    }

    public double SpinnerScale
    {
      get { return _spinnerScale; }
      set
      {
        if (value.Equals(_spinnerScale)) return;
        _spinnerScale = value;
        OnPropertyChanged();
      }
    }

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}
